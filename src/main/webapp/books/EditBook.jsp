<%-- 
    Document   : EditBook
    Created on : 12.07.2017, 10:00:18
    Author     : Маріна
--%>

<%@page import="com.komar.booksweb.service.*"%>
<%@page import="com.komar.booksweb.model.Book"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%String[] pathParts = request.getPathInfo().split("/");%>
<%int bookId = Integer.parseInt(pathParts[pathParts.length - 1]);%>
<%BookService bookService = new DBBookService();%>
<%Book book = bookService.getBookById(bookId);%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Book</title>
    </head>
    <body>
        <h1>Книга</h1>
        <form action="/BooksWeb/books/save" method="post">            
            <p>Название <input name = "title" value="<%=book.getTitle()%>" type="text"> </p>
            <p>Автор <input name = "author" value="<%=book.getAuthor()%>" type="text"/> </p>
            <p>Номер издания <input name = "edition" value="<%=book.getEdition()%>" type="number"/> </p>
            <p>Издатель <input name = "publisher" value="<%=book.getPublisher()%>" type="text"/> </p>
            <p>Дата издания <input name = "published" value="<%=book.getPublishedString()%>" type="date"/> </p>
            <p><input type="submit"/></p><input name = "bookId" value="<%= bookId %>" type="hidden"/> <br>
            </form>
    </body>
</html>
