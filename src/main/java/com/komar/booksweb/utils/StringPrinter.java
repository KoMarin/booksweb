/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.komar.booksweb.utils;

import com.komar.booksweb.model.Book;
import java.util.List;
/**
 *
 * @author Маріна
 */
public class StringPrinter {
    
    private static String buildEditionString(int edition){
        String editionString = "" + edition;

        switch (edition) {
            case 1:
                editionString += "st";
                break;
            case 2:
                editionString += "nd";
                break;
            case 3:
                editionString += "rd";
                break;
            default:
                editionString += "th";
                break;
        }

        editionString += " Edition";
        return editionString;
    }

    public static String printBook(Book book){
        String bookString;

        bookString = book.getTitle() +
                " (" + buildEditionString(book.getEdition()) +") " +
                DateParser.buildDate(book.getPublished()) + ", " +
                "by " + book.getAuthor() + ", " +
                "published by " + book.getPublisher();
             
        return bookString;
    }
    
    public static String printBookList(List<Book> books){
        String res = "";
        for (Book book : books) {
            res += printBook(book);
            res += "\n";
        }
        return res;
    }
    
    
}
