package com.komar.booksweb.model;

import com.komar.booksweb.utils.DateParser;
import com.komar.booksweb.utils.StringPrinter;
import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by Маріна on 06.07.2017.
 */
public class Book {
    private String title;
    private int edition;
    private String author;
    private String publisher;    
    private Calendar published;

    public String getTitle() {
        return title;
    }

    public int getEdition() {
        return edition;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublisher() {
        return publisher;
    }
    public Calendar getPublished() {
        return published;
    }
    
    public String getPublishedString() {
        return DateParser.buildDate(published);
    }

    public Book(String title, int edition, String author, String publisher, Calendar published) {
        this.title = title;
        this.edition = edition;
        this.author = author;
        this.publisher = publisher;
        this.published = published;
    }

    public Book(String title, int edition, String author, String publisher, String published) throws ParseException {
        this(title, edition, author, publisher, DateParser.parseDate(published));
    }
    
    public Book(){
        this("", 1, "", "", Calendar.getInstance());
    }
    
    @Override
    public String toString(){
        return StringPrinter.printBook(this);
    }
    
    public void record(Book book){
        this.author = book.getAuthor();
        this.edition = book.getEdition();
        this.published = book.getPublished();
        this.publisher = book.getPublisher();
        this.title = book.getTitle();
    }

   public static Builder newBuilder() {
        return new Book().new Builder();
    }
    
    public class Builder{
         
        public Builder withTitle(String title) {
            Book.this.title = title;
            return this;
        }
        public Builder withEdition(int edition) {
            Book.this.edition = edition;
            return this;
        }    
        public Builder withAuthor(String author) {
            Book.this.author = author;
            return this;
        }
        public Builder withPublisher(String publisher) {
            Book.this.publisher = publisher;
            return this;
        }
        public Builder withPublished(Calendar published) {
            Book.this.published = published;
            return this;
        }
        
        public Book build(){
            return Book.this;
        }
    }
    
}
