/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.komar.booksweb.servlets;

import com.komar.booksweb.service.DBBookService;
import com.komar.booksweb.utils.DateParser;
import com.komar.booksweb.model.Book;
import java.io.IOException;
import java.text.ParseException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;

/**
 *
 * @author Маріна
 */
public class BookServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private final static Logger LOGGER = LogManager.getLogger(BookServlet.class);
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            response.sendRedirect("/BooksWeb/books/list");       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response); 
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {    
        try{
            int bookId = Integer.parseInt(request.getParameter("bookId"));
            String title = request.getParameter("title");
            String author = request.getParameter("author");
            int edition = Integer.parseInt(request.getParameter("edition"));
            String publisher = request.getParameter("publisher");
            String published = request.getParameter("published");
            
            LOGGER.debug("all parameters encoded");
            
            Book book = new Book(title, edition, author, publisher, published); 
            DBBookService bookService = new DBBookService();
            bookService.saveBook(bookId, book);
            LOGGER.info(book.toString() + " saved");
            
            processRequest(request, response);
        }catch(NullPointerException ex){
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().println("Заполните все поля");
            LOGGER.error("null pointer: " + ex);
            LOGGER.debug("book: " + request.getParameter("published"));
        } catch (ParseException ex) {
            response.setContentType("text/html;charset=UTF-8");
            LOGGER.error("Date format error: "+ request.getParameter("published") + " found when " + DateParser.getFormatString() + " expected");
        }
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Helps edit or create books. Aparatly, it's goal - save changes from EditBook.jsp.";
    }// </editor-fold>

}
