<%-- 
    Document   : AllBooks
    Created on : 12.07.2017, 17:10:36
    Author     : Маріна
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AllBooks</title>
    </head>
    <body>
        <h1>Все книги:</h1>
            <c:forEach var="book" items="${result.entrySet()}">
                    <p>${book.getValue()} <a href="list/${book.getKey()}" style="appearance: button">Редактировать</a></p>
            </c:forEach>
        <a href="list/-1" style="appearance: button">Добавить книгу</a>
    </body>
</html>
