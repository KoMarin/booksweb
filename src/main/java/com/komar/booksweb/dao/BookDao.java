/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.komar.booksweb.dao;

import com.komar.booksweb.exception.BookNotFindException;
import com.komar.booksweb.model.Book;
import java.sql.SQLException;
import java.util.Map;

/**
 *
 * @author Маріна
 */
public class BookDao {
    
    H2Connector h2Connector;

    public BookDao() {
        h2Connector = new H2Connector();
    }
        
    public Book readById(int bookId) throws BookNotFindException{
        Map<Integer, Book> books = h2Connector.connectForQuery("SELECT * FROM BOOKS WHERE ID = " + bookId);
        if (books.containsKey(bookId))return books.get(bookId);
        else throw new BookNotFindException(bookId);
    }
    
    public Map<Integer, Book> readAll(){
        return h2Connector.connectForQuery("SELECT * FROM BOOKS ORDER BY ID");
    }
    
    public Map<Integer, Book> readSelectedByAuthor(String author) throws SQLException{
        return h2Connector.connectForQuery("SELECT * FROM BOOKS WHERE AUTHOR LIKE '%" + author + "%'");
    }
    
    public void createBook(Book book){
        
        h2Connector.connectForExecute("INSERT INTO BOOKS  (TITLE, AUTHOR, EDITION, PUBLISHER, PUBLISHED)" + 
                "VALUES('" + book.getTitle() +
                "', '" + book.getAuthor() + 
                "', " + book.getEdition() + 
                ", '" + book.getPublisher() + 
                "', '" + book.getPublishedString()+ "')");
    }
    
    public void updateBook(int bookId, Book book){
        h2Connector.connectForExecute("UPDATE BOOKS SET TITLE = '" + book.getTitle() +
                "', AUTHOR = '" + book.getAuthor() + 
                "', EDITION = " + book.getEdition() + 
                ", PUBLISHER = '" + book.getPublisher()+ 
                "', PUBLISHED = '" + book.getPublishedString()+ 
                "' WHERE ID = " + bookId);
    }
    
    public void deleteBook(int bookId){
        h2Connector.connectForExecute("DELETE FROM BOOKS WHERE ID = " + bookId);
    }
  
}
