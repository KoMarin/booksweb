/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.komar.booksweb.dao;

import com.komar.booksweb.model.Book;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Маріна
 */
public class H2Connector {
    
    private static  final Logger LOGGER = LogManager.getLogger(H2Connector.class);
    
    private static final String url = "jdbc:h2:~/books;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false;INIT=RUNSCRIPT FROM 'classpath:backup.sql';";
    private static final String user = "sa";
    private static final String password = "";
    
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    
    static {
        setDriver();
    }
    
    public Map<Integer, Book> connectForQuery(String query){
        
        Map<Integer, Book> books = new HashMap<>();
        try {
            resultSet = createStatement().executeQuery(query);
            LOGGER.debug("everithings's opened");
            LOGGER.debug("result: " + resultSet);
            books = buildBooks(resultSet);    
        } catch (SQLException ex) {
            LOGGER.error("DB connector error:" + ex);
        } finally{
            closeEverything();
            return books;
        }
        
    }
    
    public void connectForExecute(String execute){
        try {
            createStatement().execute(execute);
            LOGGER.debug("everithings's opened");   
        } catch (SQLException ex) {
            LOGGER.error("DB connector error:" + ex);
        } finally{
            closeEverything();
        }
    }
    
    private Statement createStatement() throws SQLException{
        connection = DriverManager.getConnection(url, user, password);
        statement = connection.createStatement();
        return statement;
    }
    
    private Map<Integer, Book> buildBooks(ResultSet resultSet) throws SQLException{
        Map books = new HashMap();
        while (resultSet.next()) {
            LOGGER.debug(resultSet);

            int bookId = resultSet.getInt("ID");
            String title = resultSet.getString("TITLE");
            String author = resultSet.getString("AUTHOR");
            int edition = resultSet.getInt("EDITION");
            String publisher = resultSet.getString("PUBLISHER");
            String published = resultSet.getString("PUBLISHED");

            try {
                books.put(bookId, new Book(title, edition, author, publisher, published));
            } catch (ParseException ex) {
                try {
                    books.put(bookId, new Book(title, edition, author, publisher, "2000-01-01"));
                } catch (ParseException ex1) {
                    LOGGER.error("parse date problem because of Marina's mistake.");
                }
                LOGGER.error("parse date problem because of DB result... default date returned (2000-01-01)");
            }
        }
        return books;
    }

    private static void setDriver(){
        try {
            Class.forName("org.h2.Driver");
            LOGGER.debug("class settled:" + Class.forName("org.h2.Driver"));
        } catch (ClassNotFoundException ex) {
            LOGGER.error("can't load driver :" + ex);
        }
    }
    
    private void closeEverything(){
        try {
            connection.close();
            statement.close();
            resultSet.close();
            LOGGER.debug("everithings's closed");
        } catch (SQLException ex) {
            LOGGER.warn("can't close: " + ex);
        } catch (NullPointerException ex){
            LOGGER.warn("something wasn't open, so, it can't be closed");
        }
    }
}
