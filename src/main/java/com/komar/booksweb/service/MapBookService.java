/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.komar.booksweb.service;

import com.komar.booksweb.utils.DateParser;
import com.komar.booksweb.model.Book;
import java.text.ParseException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.util.Map;
import java.util.HashMap;

/**
 *
 * @author Маріна
 */
public class MapBookService implements BookService{
    private static  final Logger LOGGER = LogManager.getLogger(MapBookService.class);
    
    private static Map<Integer, Book> books = createList();

    /**
     * @return the books
     */
    @Override
    public Map<Integer, Book> getBooks() {
        return books;
    }
    
    @Override
    public Book getBookById(int id){
        return ((getBooks().containsKey(id))?getBooks().get(id):new Book());  
    }
    
    private static Map<Integer, Book> createList(){
        Map<Integer, Book> books = new HashMap<>() ;
        try {
            books.put(0, new Book("Long story", 1, "Erl Gray", "empire of books", "2006-5-17"));
            books.put(1, new Book("Effective java", 2, "Joshua Bloch", "Addison-Wesley", "2008-04-28"));
         } catch (ParseException ex) {
            LOGGER.error("!!!Wrong parse format for date!!! " + DateParser.getFormatString() + " expected");
        }

        return books;
    }
    
    private boolean findByAuthor(Book book, String toCompare){
        return book.getAuthor().toLowerCase().contains(toCompare.toLowerCase());
    }
    
    @Override
    public Map<Integer, Book> searchByAuthor(String toCompare){
        Map<Integer, Book> searchResult = new HashMap<Integer, Book>();
        for (Map.Entry<Integer, Book> book : books.entrySet()) {
            if (findByAuthor(book.getValue(), toCompare)) searchResult.put(book.getKey(), book.getValue());
            
        } 
        return searchResult;
    }
    
    @Override
    public void saveBook(int BookId, Book book){
        if (books.containsKey(BookId)) books.get(BookId).record(book);
        else books.put(BookId, book);
    }
}
