<%-- 
    Document   : SearchJsp
    Created on : 11.07.2017, 13:22:35
    Author     : Маріна
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search</title>
    </head>
    <title>Поиск</title>
    <body>
        <h1>Поиск книг</h1>
            <form action = "search" method="get">
                <input type="text" name="author"/>
                <input type="submit" value="Поиск"/>
                <c:forEach var="book" items="${result.entrySet()}">
                    <p>${book.getValue()} <a href="search/${book.getKey()}" style="appearance: button">Редактировать</a></p>
                </c:forEach>
            </form>
    </body>
</html>
