/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.komar.booksweb.servlets;

import com.komar.booksweb.service.DBBookService;
import com.komar.booksweb.utils.StringPrinter;
import com.komar.booksweb.model.Book;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
/**
 *
 * @author Маріна
 */

public class ListServlet extends HttpServlet {
    
    static final Logger LOGGER = LogManager.getLogger(ListServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        LOGGER.info("Load /list: print all books...");
         request.getRequestDispatcher("AllBooks.jsp").forward(request, response);
        LOGGER.info("/list loaded; all books are printed.");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DBBookService bookService = new DBBookService();
        LOGGER.debug("request for all books from db is staring..");
        Map<Integer, Book> books = bookService.getBooks();
        LOGGER.info("books loaded:" + StringPrinter.printBookList(new ArrayList(books.values())));
        request.setAttribute("result", books);
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "When it eventually start working, it'll return you liiittle list of books";
    }// </editor-fold>

}
