package com.komar.booksweb.service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.komar.booksweb.exception.BookNotFindException;
import com.komar.booksweb.dao.BookDao;
import com.komar.booksweb.utils.StringPrinter;
import com.komar.booksweb.model.Book;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;

/**
 *
 * @author Маріна
 */
public class DBBookService implements BookService{
    
    private static  final Logger LOGGER = LogManager.getLogger(DBBookService.class);
    
    private StringPrinter printer;
    private BookDao dbConnector;
    
    public DBBookService(){
        printer = new StringPrinter();
        dbConnector = new BookDao();
    }
    
    @Override
    public Map<Integer, Book> getBooks() {
        LOGGER.debug("BookServlet Recieves request ant give it to dbConnector");
        return dbConnector.readAll();
    }
    
    @Override
    public Book getBookById(int id){
        try {
            return dbConnector.readById(id);
        } catch (BookNotFindException ex) {
            LOGGER.info("we return you new book, because required's not found");
            return new Book();
        }
    }
        
    @Override
    public Map<Integer, Book> searchByAuthor(String toCompare){
        try {
            return dbConnector.readSelectedByAuthor(toCompare);
        } catch (SQLException ex) {
            LOGGER.error("DB conection error: " + ex + ". empty map returned");
            return new HashMap();
        }
    }
    
    @Override
    public void saveBook(int BookId, Book book){
        if (BookId < 0) dbConnector.createBook(book);
        else dbConnector.updateBook(BookId, book);
    } 

}
