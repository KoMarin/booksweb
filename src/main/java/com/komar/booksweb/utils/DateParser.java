/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.komar.booksweb.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author Маріна
 */
public class DateParser {
   
    private static final String formatString = "YYYY-mm-dd";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(formatString, Locale.ENGLISH);
    
    public static String getFormatString(){
        return formatString;
    }
    
    public static Calendar parseDate(String dateString) throws ParseException{
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse(dateString));
        return calendar;
    }
    
    public static String buildDate(Calendar date){
        return dateFormat.format(date.getTime());
    }
}
