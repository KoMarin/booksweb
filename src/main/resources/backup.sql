CREATE TABLE if not exists BOOKS(ID INT auto_increment primary key, TITLE VARCHAR, AUTHOR VARCHAR, EDITION INT, PUBLISHER VARCHAR, PUBLISHED DATETIME);            
INSERT INTO BOOKS(TITLE, AUTHOR, EDITION, PUBLISHER, PUBLISHED) VALUES
('Long Story', 'Erl Gray', 1, 'empire of books', TIMESTAMP '2006-05-17 00:00:00.0'),
('Effective java', 'Joshua Bloch', 2, 'Addison-Wesley', TIMESTAMP '2008-04-28 00:00:00.0'),
('The colour of magic', 'Terry Pratchett', 3, STRINGDECODE('Discworld Collector\ufffds Library'), TIMESTAMP '2016-02-06 00:00:00.0');          
