/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.komar.booksweb.service;

import com.komar.booksweb.model.Book;
import java.util.Map;

/**
 *
 * @author Маріна
 */
public interface BookService {
    public Map<Integer, Book> getBooks(); 
    public Book getBookById(int id);
    public Map<Integer, Book> searchByAuthor(String toCompare);
    public void saveBook(int BookId, Book book);
}
