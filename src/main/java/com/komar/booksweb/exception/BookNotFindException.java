/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.komar.booksweb.exception;

/**
 *
 * @author Маріна
 */
public class BookNotFindException extends Exception{
    private int id;

    public BookNotFindException(int id) {
        super("Book with id " + id + " is not exist");
        this.id = id;
    }    
}
